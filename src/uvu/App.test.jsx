import { suite } from 'uvu'
import * as assert from 'uvu/assert'
import { screen, render, fireEvent } from 'solid-testing-library'
import { MyComponent } from '../App'
// import arbitraryCode from '../arbitraryJSFile' // Doesn't work
import moreArbitraryCode from '../arbitraryJSXFile' // Works fine

const isInDom = node => !!node.parentNode
  && (node.parentNode === document || isInDom (node.parentNode))

const test = suite ('MyComponent')

test ('changes text on click', async () => {
  await render (() => <MyComponent />)
  const component = await screen.findByRole ('button', { name: 'Click me!' })
  assert.ok (isInDom (component))
  fireEvent.click (component)
  assert.ok (isInDom (await screen.findByRole ('button', { name: 'Test this!' })))
})

test.run ()
