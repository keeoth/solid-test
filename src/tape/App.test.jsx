import test from 'tape'
import { screen, render, fireEvent } from 'solid-testing-library'
import { MyComponent } from '../App'
// import arbitraryCode from '../arbitraryJSFile' // Doesn't work
import moreArbitraryCode from '../arbitraryJSXFile' // Works fine

// test ('test', t => {
//   t.plan (1)
//   t.ok (false)
// })

const isInDom = node => !!node.parentNode
  && (node.parentNode === document || isInDom (node.parentNode))

test ('changes text on click', async t => {
  t.plan (2)
  await render (() => <MyComponent />)
  const component = await screen.findByRole ('button', { name: 'Click me!' })
  t.ok (isInDom (component))
  fireEvent.click (component)
  t.ok (isInDom (await screen.findByRole ('button', { name: 'Test this!' })))
})
