// eslint-disable-next-line functional/immutable-data
module.exports = {
  env: {
    browser: true,
    es2021: true,
    'jest/globals': true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
    'plugin:solid/recommended',
    'plugin:functional/external-recommended',
    'plugin:functional/recommended',
    'plugin:functional/stylistic',
    'plugin:functional/all',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['react', 'jest', 'functional', 'solid'],
  settings: {
    'import/resolver': {
      alias: {
        map: [['#', './src']],
        extensions: ['.js', '.jsx', '.json'],
      },
    },
  },
  rules: {
    'react/destructuring-assignment': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/prop-types': 'off',
    'func-call-spacing': [
      'error',
      'always',
      { allowNewlines: true },
    ],
    'no-unexpected-multiline': 0,
    'import/no-extraneous-dependencies': [
      'error',
      { devDependencies: ['**/*.test.jsx'] },
    ],
    'no-spaced-func': 0,
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'always',
        named: 'always',
        asyncArrow: 'always',
      },
    ],
    'functional/no-expression-statement': [0, { ignoreVoid: true }],
    'functional/functional-parameters': [
      'error',
      {
        allowArgumentsKeyword: false,
        allowRestParameter: false,
        enforceParameterCount: false,
      },
    ],
    'functional/no-conditional-statement': [
      'error',
      {
        allowReturningBranches: true,
      },
    ],
    indent: ['error', 2],
    'no-trailing-spaces': 'error',
    'linebreak-style': ['error', 'unix'],
    'max-len': ['error', { code: 200, tabWidth: 2 }],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
    'comma-dangle': ['error', 'always-multiline'],
    'arrow-parens': ['error', 'as-needed'],
    'react/no-unknown-property': [2, { ignore: ['class', 'for'] }],
  },
}
