// eslint-disable-next-line import/no-extraneous-dependencies
import { defineConfig } from 'vite';
// eslint-disable-next-line import/no-extraneous-dependencies
import solidPlugin from 'vite-plugin-solid';
import solidSvg from "vite-plugin-solid-svg"
import path from 'path';

export default defineConfig({
  resolve: {
    alias: {
      '#': path.resolve(__dirname, '/src'),
    },
  },
  plugins: [solidPlugin(), solidSvg()],
  build: {
    target: 'esnext',
    polyfillDynamicImport: false,
  },
});
